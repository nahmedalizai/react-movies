import {LOADER} from './actionType'

export const loaderState = (loading) => {
    return {
        type : LOADER,
        payload : loading
    }
}