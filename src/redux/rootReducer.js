import {combineReducers} from 'redux'
import loadingActionReducer from './actions/loadingActionReducer'

const rootReducer = combineReducers ({
    loadingState: loadingActionReducer
})

export default rootReducer