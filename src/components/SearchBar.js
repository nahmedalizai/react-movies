import React, { useState } from 'react';
import axios from 'axios';
import config from 'react-global-configuration';
import { useHistory } from 'react-router';
import { toaster } from 'evergreen-ui';
import { useDispatch } from 'react-redux';
import { loaderState } from '../redux';
import { IconButton, InputBase, Divider, Paper } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Search } from '@material-ui/icons';

const useStyles = makeStyles((theme) => ({
    button: {
        margin: theme.spacing(1),
    },
    root: {
        display: 'flex',
        alignItems: 'center',
    },
    input: {
        marginLeft: theme.spacing(1),
        flex: 1,
    },
    iconButton: {
        padding: 10,
    },
    divider: {
        height: 28,
        margin: 4,
    },
}));

function SearchBar() {
    const classes = useStyles();
    const dispatch = useDispatch()
    const history = useHistory();
    const [searchValue, setSearchValue] = useState('');

    const onSearchChange = (event) => setSearchValue(event.target.value);

    const searchMovie = async (event) => {
        event.preventDefault();
        if (searchValue !== null && searchValue !== '') {
            dispatch(loaderState(true))
            await axios({
                method: 'GET',
                url: config.get('apiurl'),
                params: {
                    r: 'json',
                    t: searchValue
                },
                headers: {
                    'Content-Type': 'application/octet-stream',
                    'x-rapidapi-host': 'movie-database-imdb-alternative.p.rapidapi.com',
                    'x-rapidapi-key': process.env.REACT_APP_MOVIES_API_KEY
                }
            })
                .then((response) => {
                    dispatch(loaderState(false))
                    if (response != null && response.status === 200) {
                        if (response && response.data) {
                            setSearchValue('')
                            history.push('/movie/' + response.data.imdbID, response.data);
                        }
                    }
                })
                .catch((error) => {
                    dispatch(loaderState(false))
                    console.log(error);
                    toaster.danger(
                        'Something went wrong.. please try later', {
                        id: 'forbidden-action'
                    }
                    );
                })
        }
        else {
            toaster.warning(
                'Please enter movie name to search', {
                id: 'forbidden-action'
            }
            );
        }
    }

    return (
        <Paper component="form" className={classes.root}>
            <InputBase
                className={classes.input}
                placeholder="Search Movie"
                inputProps={{ 'aria-label': 'search movie' }}
                onChange={(event) => onSearchChange(event)}
                value={searchValue}
                required={true}
                type="text"
            />
            <Divider className={classes.divider} orientation="vertical" />
            <IconButton type="submit" className={classes.iconButton} aria-label="search" onClick={(event) => searchMovie(event)}>
                <Search />
            </IconButton>
        </Paper>
    )
}

export default SearchBar;