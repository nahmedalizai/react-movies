import React from 'react'
import LoadingOverlay from 'react-loading-overlay'

function Spinner(props) {
    return (
        <LoadingOverlay
            active={props.isActive}
            spinner
        >
        </LoadingOverlay>
    )
}

export default Spinner
