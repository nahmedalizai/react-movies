import React from 'react';
import { useHistory } from 'react-router'; //useParams, useLocation

function Movie(props) {

    // const params = useParams();
    // const location = useLocation();
    const history = useHistory();
    const data = props.location.state;
    const handleBackClick = () => {
        history.goBack();
    }

    return (
        <div>
            <div className="row">
                <table className="mb-3 subHeader">
                    <tbody>
                        <tr>
                            <td>
                                <button className="movieBackButton" onClick={handleBackClick}>Back</button>
                            </td>
                            <td>
                                <h5 className="titleBadge">{'Title : ' + data.Title}</h5>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div className="row">
                <div className="col-lg-3 col-md-6 mb-3">
                    <img className="card-img-top"
                        src={data.Poster}
                        alt="" />
                </div>
                <div className="col mb-3 detailsContainer">
                    <div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Movie;