import React from 'react';
import { Link } from 'react-router-dom';
import { Navbar, Nav } from 'react-bootstrap';
import SearchBar from './SearchBar';
import Dialog from './Dialog';
import { LockOpenOutlined, LocalMovies, Home } from '@material-ui/icons';
import { Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import LoginTabs from './LoginTabs';
import DropMenu from './Dropdown'

const config = {
    formTitle: 'Login',
    buttonTitle: 'Login/Signup',
    icon: <LockOpenOutlined />,
    buttonType: 'icon',
    tooltip: 'Login / Signup',
    content: <LoginTabs />
}

const useStyles = makeStyles((theme) => ({
    button: {
        margin: theme.spacing(1),
    },
}));

function Navigatonbar() {
    const classes = useStyles();
    return (
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark" sticky="top">
            <Navbar.Brand href="/">React Movies</Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
                <Nav className="mr-auto">
                    <Link to='/home'>
                        <Button
                            variant="contained"
                            color="default"
                            className={classes.button}
                            startIcon={<Home />}
                        >
                            Home
                        </Button>
                    </Link>
                    <Link to='/mostwatched'>
                        <Button
                            variant="contained"
                            color="default"
                            className={classes.button}
                            startIcon={<LocalMovies />}
                        >
                            Most Watched
                    </Button>
                    </Link>
                    <DropMenu />
                    <Dialog config={config} />
                    {/* <Link to='/login'> Login/Signup </Link> */}
                </Nav>
                <Nav>
                    <SearchBar />
                </Nav>
            </Navbar.Collapse>
        </Navbar>)
}

export default Navigatonbar;