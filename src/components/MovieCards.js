import React from 'react';
import { useHistory } from 'react-router';
import config from 'react-global-configuration';
import axios from 'axios';
import { toaster } from 'evergreen-ui';
import {useDispatch} from 'react-redux';
import { loaderState } from '../redux';

function MovieCards(props) {
    const dispatch = useDispatch()
    const history = useHistory();

    const handleClick = async (event) => {
        if (event.target.id) {
            dispatch(loaderState(true))
            const id = event.target.id;
            await axios({
                method: 'GET',
                url: config.get('apiurl'),
                params: {
                    i: event.target.id,
                    plot: 'full'
                },
                headers: {
                    'Content-Type': 'application/octet-stream',
                    'x-rapidapi-host': 'movie-database-imdb-alternative.p.rapidapi.com',
                    'x-rapidapi-key': process.env.REACT_APP_MOVIES_API_KEY
                }
            })
                .then((response) => {
                    dispatch(loaderState(false))
                    if (response != null && response.status === 200) {
                        if (response && response.data) {
                            history.push('/movie/' + id, response.data);
                        }
                    }
                })
                .catch((error) => {
                    dispatch(loaderState(false))
                    console.log(error);
                    toaster.danger(
                        'Something went wrong.. please try later', {
                        id: 'forbidden-action'
                    }
                    );
                })
        }
        else
            console.log('id is null');
    }

    return (<div className="row text-center">
        <div className="col-lg-3 col-md-6 mb-4">
            <img className="card-img-top" id="tt2527338"
                onClick={(event) => handleClick(event)}
                src={process.env.REACT_APP_MOVIES_POSTER_1}
                alt="" />
        </div>
        <div className="col-lg-3 col-md-6 mb-4">
            <img className="card-img-top" id="tt6450804"
                onClick={(event) => handleClick(event)}
                src={process.env.REACT_APP_MOVIES_POSTER_2}
                alt="" />
        </div>
        <div className="col-lg-3 col-md-6 mb-4">
            <img className="card-img-top" id="tt7286456"
                onClick={(event) => handleClick(event)}
                src={process.env.REACT_APP_MOVIES_POSTER_3}
                alt="" />
        </div>
        <div className="col-lg-3 col-md-6 mb-4">
            <img className="card-img-top" id="tt1206885"
                onClick={(event) => handleClick(event)}
                src={process.env.REACT_APP_MOVIES_POSTER_4}
                alt="" />
        </div>
    </div>)
}

export default MovieCards;