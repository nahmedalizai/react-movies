import React from 'react';
import ControlledCarousel from './Carousel.js';
import MovieCards from './MovieCards.js';
import LoadingOverlay from 'react-loading-overlay';
import {connect} from 'react-redux';

function LandingPage(props) {
    return (<div>
        <LoadingOverlay
            active={props.isActive}
            spinner
            text=''
        >
            <ControlledCarousel />
            <MovieCards props={props} />
        </LoadingOverlay>
    </div>)
}

const mapStateToProps = state => {
    return {
        isActive:  state.loadingState.isLoading
    }
}

export default connect(mapStateToProps)(LandingPage);