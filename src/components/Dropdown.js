import React from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';

const useStyles = makeStyles((theme) => ({
    button: {
        margin: theme.spacing(1),
    },
}));

export default function SimpleMenu() {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <div>
            <Button 
            variant="contained"
            color="default"
            className={classes.button}
            startIcon={<MenuIcon />}
            aria-controls="simple-menu" 
            aria-haspopup="true" 
            onClick={handleClick}>
                Genre
            </Button>
            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <Link to='/home'>
                    <MenuItem onClick={handleClose}>Action</MenuItem>
                </Link>
                <Link to='/home'>
                    <MenuItem onClick={handleClose}>Drama</MenuItem>
                </Link>
                <Link to='/home'>
                    <MenuItem onClick={handleClose}>History</MenuItem>
                </Link>
                <Link to='/home'>
                    <MenuItem onClick={handleClose}>Comedy</MenuItem>
                </Link>
                <Link to='/home'>
                    <MenuItem onClick={handleClose}>All</MenuItem>
                </Link>
            </Menu>
        </div>
    );
}