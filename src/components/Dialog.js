import React, { useState } from 'react'
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import MuiDialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
//import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import ToolTip from '@material-ui/core/Tooltip';
const styles = (theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(2),
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing(1),
        top: theme.spacing(1),
        color: theme.palette.grey[500],
    },
});

const DialogTitle = withStyles(styles)((props) => {
    const { children, classes, onClose, ...other } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root} {...other}>
            <Typography variant="h6">{children}</Typography>
            {onClose ? (
                <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles((theme) => ({
    root: {
        padding: theme.spacing(2),
    },
}))(MuiDialogContent);

// const DialogActions = withStyles((theme) => ({
//     root: {
//         margin: 0,
//         padding: theme.spacing(1),
//     },
// }))(MuiDialogActions);

const Dialog = (props) => {
    const [showDialog, setShowDialog] = useState(false)
    const handleOpenDialog = () => {
        setShowDialog(true)
    }
    const handleCloseDialog = () => {
        setShowDialog(false)
    }
    return (
        <div>
            <ToolTip title={props.config.tooltip}>
                {props.config.buttonType === 'icon' ?
                    <IconButton aria-label={props.config.buttonTitle} onClick={handleOpenDialog}>
                        {props.config.icon}
                    </IconButton> :
                    <Button color="default" onClick={handleOpenDialog}>
                        {props.config.buttonTitle}
                    </Button>
                }
            </ToolTip>
            <MuiDialog onClose={handleCloseDialog} aria-labelledby="customized-dialog-title" open={showDialog}>
                {/* <DialogTitle id="customized-dialog-title" onClose={handleCloseDialog}>
                    {props.config.formTitle}
                </DialogTitle> */}
                <DialogContent dividers>
                    {props.config.content}
                </DialogContent>
                {/* <DialogActions>
                    <Button autoFocus onClick={handleCloseDialog} color="primary">
                        Action
          </Button>
                </DialogActions> */}
            </MuiDialog>
        </div>
    )
}

export default Dialog
