import React from 'react';
import './App.css';
import './config';
import Navigatonbar from './components/Navbar';
import LandingPage from './components/LandingPage';
import Footer from './components/Footer.js';
import Home from './components/Home.js';
import MostWatched from './components/MostWatched';
import NotFound from './components/NotFound';
//import Login from './components/Login.js';
import Movie from './components/Movie.js';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './redux/store';
import history from './components/History';

function App() {

  return (
    <div className="container">
      <Provider store={store}>
        <Router history={history}>
          <Navigatonbar />
          <Switch>
            <Route path="/" exact component={LandingPage} />
            <Route exact path="/home" component={Home} />
            <Route path="/mostwatched" exact component={MostWatched} />
            {/* <Route path="/login" exact component={Login} /> */}
            <Route path="/movie/:id" exact component={Movie} />
            <Route path="*" exact component={NotFound} />
          </Switch>
        </Router>
        <Footer />
      </Provider>
    </div>);
}

export default App;
